package testsuite;
import org.testng.annotations.Test;
import PageObjects.*;
import utilities.PageObjectBase;
import org.openqa.selenium.support.PageFactory;
import utilities.Configurations;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import utilities.TestReport;
import java.io.IOException;
import org.testng.Reporter;
import utilities.DataUtil;


/** Conformiq generated test case
	Open_New_Account1__4_
*/
public class Open_New_Account1__4_ extends PageObjectBase
{

	public Open_New_Account1__4_()
	{
	}

	private TestReport testReport= new TestReport();


	private StringBuilder overallTestData= new StringBuilder();


	@Test(dataProvider="TestData")
	public void test(final String Step_1_Username_TEXTBOX_Status,final String Step_1_Username_TEXTBOX_Verification,final String Step_1_passowrd_TEXTBOX_Status,final String Step_1_passowrd_TEXTBOX_Verification,final String Step_1_Error_Message_LABEL_Status,final String Step_2_Username_TEXTBOX,final String Step_2_passowrd_TEXTBOX,final String Step_3_Open_New_Account_HYPERLINK_Status,final String Step_3_Transfer_Funds_HYPERLINK_Status,final String Step_3_Logout_HYPERLINK_Status,final String Step_5_Account_Tupe_DROPDOWN_Status,final String Step_5_Account_Tupe_DROPDOWN_Verification,final String Step_5_Existing_Account_DROPDOWN_Status,final String Step_5_Existing_Account_DROPDOWN_Verification,final String Step_5_OPEN_NEW_ACCOUNT_BUTTON_Status,final String Step_6_Account_Tupe_DROPDOWN,final String Step_6_Existing_Account_DROPDOWN) throws Exception

	{

	Parabank_Login_Page parabank_login_page_init=PageFactory.initElements(driver, Parabank_Login_Page.class);

	Account_Services_Page account_services_page_init=PageFactory.initElements(driver, Account_Services_Page.class);

	Screen_Widget_Page screen_widget_page_init=PageFactory.initElements(driver, Screen_Widget_Page.class);

	Open_New_Account_Page open_new_account_page_init=PageFactory.initElements(driver, Open_New_Account_Page.class);

	Transfer_Funds_Page transfer_funds_page_init=PageFactory.initElements(driver, Transfer_Funds_Page.class);

	Order_Page order_page_init=PageFactory.initElements(driver, Order_Page.class);
	testReport.createTesthtmlHeader(overallTestData);

	testReport.createHead(overallTestData);

	testReport.putLogo(overallTestData);

	float ne = (float) 0.0;

	testReport.generateGeneralInfo(overallTestData, "Open_New_Account1__4_", "TC_Open_New_Account1__4_", "",ne);

	testReport.createStepHeader();

	//External Circumstances


	Reporter.log("Step - 1- verify Parabank Login screen");

	testReport.fillTableStep("Step 1", "verify Parabank Login screen");

	parabank_login_page_init.verify_Username_Status(Step_1_Username_TEXTBOX_Status);

	parabank_login_page_init.verify_Username(Step_1_Username_TEXTBOX_Verification);
	parabank_login_page_init.verify_passowrd_Status(Step_1_passowrd_TEXTBOX_Status);

	parabank_login_page_init.verify_passowrd(Step_1_passowrd_TEXTBOX_Verification);
	parabank_login_page_init.verify_Error_Message_Status(Step_1_Error_Message_LABEL_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Open_New_Account1__4_","Step_1");

	Reporter.log("Step - 2- Fill Customer Login form Parabank Login screen");

	testReport.fillTableStep("Step 2", "Fill Customer Login form Parabank Login screen");

	parabank_login_page_init.set_Username(Step_2_Username_TEXTBOX);
	parabank_login_page_init.set_passowrd(Step_2_passowrd_TEXTBOX);
	getScreenshot(driver,Configurations.screenshotLocation , "Open_New_Account1__4_","Step_2");

	Reporter.log("Step - 3- verify Account Services screen");

	testReport.fillTableStep("Step 3", "verify Account Services screen");

	account_services_page_init.verify_Open_New_Account_Status(Step_3_Open_New_Account_HYPERLINK_Status);

	account_services_page_init.verify_Transfer_Funds_Status(Step_3_Transfer_Funds_HYPERLINK_Status);

	account_services_page_init.verify_Logout_Status(Step_3_Logout_HYPERLINK_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Open_New_Account1__4_","Step_3");

	Reporter.log("Step - 4- click Open New Account hyperlink Account Services screen");

	testReport.fillTableStep("Step 4", "click Open New Account hyperlink Account Services screen");

	account_services_page_init.click_Open_New_Account();
	getScreenshot(driver,Configurations.screenshotLocation , "Open_New_Account1__4_","Step_4");

	Reporter.log("Step - 5- verify Open New Account screen");

	testReport.fillTableStep("Step 5", "verify Open New Account screen");

	open_new_account_page_init.verify_Account_Tupe_Status(Step_5_Account_Tupe_DROPDOWN_Status);

	open_new_account_page_init.verify_Account_Tupe(Step_5_Account_Tupe_DROPDOWN_Verification);
	open_new_account_page_init.verify_Existing_Account_Status(Step_5_Existing_Account_DROPDOWN_Status);

	open_new_account_page_init.verify_Existing_Account(Step_5_Existing_Account_DROPDOWN_Verification);
	open_new_account_page_init.verify_OPEN_NEW_ACCOUNT_Status(Step_5_OPEN_NEW_ACCOUNT_BUTTON_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Open_New_Account1__4_","Step_5");

	Reporter.log("Step - 6- Fill Open New Account form Open New Account screen");

	testReport.fillTableStep("Step 6", "Fill Open New Account form Open New Account screen");

	open_new_account_page_init.select_Account_Tupe(Step_6_Account_Tupe_DROPDOWN);
	open_new_account_page_init.select_Existing_Account(Step_6_Existing_Account_DROPDOWN);
	getScreenshot(driver,Configurations.screenshotLocation , "Open_New_Account1__4_","Step_6");

	Reporter.log("Step - 7- click OPEN NEW ACCOUNT button Open New Account screen");

	testReport.fillTableStep("Step 7", "click OPEN NEW ACCOUNT button Open New Account screen");

	open_new_account_page_init.click_OPEN_NEW_ACCOUNT();
	getScreenshot(driver,Configurations.screenshotLocation , "Open_New_Account1__4_","Step_7");

	Reporter.log("Step - 8- click Logout hyperlink Account Services screen");

	testReport.fillTableStep("Step 8", "click Logout hyperlink Account Services screen");

	account_services_page_init.click_Logout();
	getScreenshot(driver,Configurations.screenshotLocation , "Open_New_Account1__4_","Step_8");
	}
	@DataProvider(name = "TestData")
	public Object[][] getData() {
	return DataUtil.getDataFromSpreadSheet("TestData.xlsx", "TCID_16");
}
	@AfterTest
	public void export(){
		testReport.appendtestData(overallTestData);
		testReport.closeStepTable();
		testReport.closeTestHTML(overallTestData);
		driver.close();
		try {
			testReport.writeTestReporthtml(overallTestData, "Open_New_Account1__4_");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
