package testsuite;
import org.testng.annotations.Test;
import PageObjects.*;
import utilities.PageObjectBase;
import org.openqa.selenium.support.PageFactory;
import utilities.Configurations;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import utilities.TestReport;
import java.io.IOException;
import org.testng.Reporter;
import utilities.DataUtil;


/** Conformiq generated test case
	Transfer_Funds__3_
*/
public class Transfer_Funds__3_ extends PageObjectBase
{

	public Transfer_Funds__3_()
	{
	}

	private TestReport testReport= new TestReport();


	private StringBuilder overallTestData= new StringBuilder();


	@Test(dataProvider="TestData")
	public void test(final String Step_1_Username_TEXTBOX_Status,final String Step_1_Username_TEXTBOX_Verification,final String Step_1_passowrd_TEXTBOX_Status,final String Step_1_passowrd_TEXTBOX_Verification,final String Step_1_Error_Message_LABEL_Status,final String Step_2_Username_TEXTBOX,final String Step_2_passowrd_TEXTBOX,final String Step_3_Open_New_Account_HYPERLINK_Status,final String Step_3_Transfer_Funds_HYPERLINK_Status,final String Step_3_Logout_HYPERLINK_Status,final String Step_5_Amount_TEXTBOX_Status,final String Step_5_Amount_TEXTBOX_Verification,final String Step_5_From_Account_DROPDOWN_Status,final String Step_5_From_Account_DROPDOWN_Verification,final String Step_5_To_Account_DROPDOWN_Status,final String Step_5_To_Account_DROPDOWN_Verification,final String Step_5_Transfer_Funds_BUTTON_Status,final String Step_6_Amount_TEXTBOX,final String Step_6_From_Account_DROPDOWN,final String Step_6_To_Account_DROPDOWN) throws Exception

	{

	Parabank_Login_Page parabank_login_page_init=PageFactory.initElements(driver, Parabank_Login_Page.class);

	Account_Services_Page account_services_page_init=PageFactory.initElements(driver, Account_Services_Page.class);

	Screen_Widget_Page screen_widget_page_init=PageFactory.initElements(driver, Screen_Widget_Page.class);

	Open_New_Account_Page open_new_account_page_init=PageFactory.initElements(driver, Open_New_Account_Page.class);

	Transfer_Funds_Page transfer_funds_page_init=PageFactory.initElements(driver, Transfer_Funds_Page.class);

	Order_Page order_page_init=PageFactory.initElements(driver, Order_Page.class);
	testReport.createTesthtmlHeader(overallTestData);

	testReport.createHead(overallTestData);

	testReport.putLogo(overallTestData);

	float ne = (float) 0.0;

	testReport.generateGeneralInfo(overallTestData, "Transfer_Funds__3_", "TC_Transfer_Funds__3_", "",ne);

	testReport.createStepHeader();

	//External Circumstances


	Reporter.log("Step - 1- verify Parabank Login screen");

	testReport.fillTableStep("Step 1", "verify Parabank Login screen");

	parabank_login_page_init.verify_Username_Status(Step_1_Username_TEXTBOX_Status);

	parabank_login_page_init.verify_Username(Step_1_Username_TEXTBOX_Verification);
	parabank_login_page_init.verify_passowrd_Status(Step_1_passowrd_TEXTBOX_Status);

	parabank_login_page_init.verify_passowrd(Step_1_passowrd_TEXTBOX_Verification);
	parabank_login_page_init.verify_Error_Message_Status(Step_1_Error_Message_LABEL_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Transfer_Funds__3_","Step_1");

	Reporter.log("Step - 2- Fill Customer Login form Parabank Login screen");

	testReport.fillTableStep("Step 2", "Fill Customer Login form Parabank Login screen");

	parabank_login_page_init.set_Username(Step_2_Username_TEXTBOX);
	parabank_login_page_init.set_passowrd(Step_2_passowrd_TEXTBOX);
	getScreenshot(driver,Configurations.screenshotLocation , "Transfer_Funds__3_","Step_2");

	Reporter.log("Step - 3- verify Account Services screen");

	testReport.fillTableStep("Step 3", "verify Account Services screen");

	account_services_page_init.verify_Open_New_Account_Status(Step_3_Open_New_Account_HYPERLINK_Status);

	account_services_page_init.verify_Transfer_Funds_Status(Step_3_Transfer_Funds_HYPERLINK_Status);

	account_services_page_init.verify_Logout_Status(Step_3_Logout_HYPERLINK_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Transfer_Funds__3_","Step_3");

	Reporter.log("Step - 4- click Transfer Funds hyperlink Account Services screen");

	testReport.fillTableStep("Step 4", "click Transfer Funds hyperlink Account Services screen");

	account_services_page_init.click_Transfer_Funds();
	getScreenshot(driver,Configurations.screenshotLocation , "Transfer_Funds__3_","Step_4");

	Reporter.log("Step - 5- verify Transfer Funds screen");

	testReport.fillTableStep("Step 5", "verify Transfer Funds screen");

	transfer_funds_page_init.verify_Amount_Status(Step_5_Amount_TEXTBOX_Status);

	transfer_funds_page_init.verify_Amount(Step_5_Amount_TEXTBOX_Verification);
	transfer_funds_page_init.verify_From_Account_Status(Step_5_From_Account_DROPDOWN_Status);

	transfer_funds_page_init.verify_From_Account(Step_5_From_Account_DROPDOWN_Verification);
	transfer_funds_page_init.verify_To_Account_Status(Step_5_To_Account_DROPDOWN_Status);

	transfer_funds_page_init.verify_To_Account(Step_5_To_Account_DROPDOWN_Verification);
	transfer_funds_page_init.verify_Transfer_Funds_Status(Step_5_Transfer_Funds_BUTTON_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Transfer_Funds__3_","Step_5");

	Reporter.log("Step - 6- Fill Transfer Funds form Transfer Funds screen");

	testReport.fillTableStep("Step 6", "Fill Transfer Funds form Transfer Funds screen");

	transfer_funds_page_init.set_Amount(Step_6_Amount_TEXTBOX);
	transfer_funds_page_init.select_From_Account(Step_6_From_Account_DROPDOWN);
	transfer_funds_page_init.select_To_Account(Step_6_To_Account_DROPDOWN);
	getScreenshot(driver,Configurations.screenshotLocation , "Transfer_Funds__3_","Step_6");

	Reporter.log("Step - 7- click Transfer Funds button Transfer Funds screen");

	testReport.fillTableStep("Step 7", "click Transfer Funds button Transfer Funds screen");

	transfer_funds_page_init.click_Transfer_Funds();
	getScreenshot(driver,Configurations.screenshotLocation , "Transfer_Funds__3_","Step_7");

	Reporter.log("Step - 8- click Logout hyperlink Account Services screen");

	testReport.fillTableStep("Step 8", "click Logout hyperlink Account Services screen");

	account_services_page_init.click_Logout();
	getScreenshot(driver,Configurations.screenshotLocation , "Transfer_Funds__3_","Step_8");
	}
	@DataProvider(name = "TestData")
	public Object[][] getData() {
	return DataUtil.getDataFromSpreadSheet("TestData.xlsx", "TCID_15");
}
	@AfterTest
	public void export(){
		testReport.appendtestData(overallTestData);
		testReport.closeStepTable();
		testReport.closeTestHTML(overallTestData);
		driver.close();
		try {
			testReport.writeTestReporthtml(overallTestData, "Transfer_Funds__3_");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
