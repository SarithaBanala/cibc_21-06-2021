package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Screen_Widget_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "TB")
	public static WebElement TB;

public void verify_TB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(TB.getAttribute("value"),data);
	}

}

public void verify_TB_Status(String data){
		//Verifies the Status of the TB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(TB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(TB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!TB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!TB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_TB(String data){
		TB.clear();
		TB.sendKeys(data);
}

@FindBy(how= How.ID, using = "CB")
	public static WebElement CB;

public void verify_CB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(CB.getAttribute("value"),data);
	}

}

public void verify_CB_Status(String data){
		//Verifies the Status of the CB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(CB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(CB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!CB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!CB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_CB(String data){
		if(CB.isSelected());
			CB.click();
}

@FindBy(how= How.ID, using = "DD")
	public static WebElement DD;

public void verify_DD(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(DD.getAttribute("value"),data);
	}

}

public void verify_DD_Status(String data){
		//Verifies the Status of the DD
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(DD.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(DD.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!DD.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!DD.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_DD(String data){
		Select dropdown= new Select(DD);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "RB")
	public static WebElement RB;

public void verify_RB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(RB.getAttribute("name"),data);
	}

}

public void verify_RB_Status(String data){
		//Verifies the Status of the RB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(RB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(RB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!RB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!RB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
@FindBy(how= How.ID, using = "Calendar")
	public static WebElement Calendar;

public void verify_Calendar(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Calendar.getAttribute("value"),data);
	}

}

public void verify_Calendar_Status(String data){
		//Verifies the Status of the Calendar
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Calendar.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Calendar.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Calendar.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Calendar.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Calendar(String data){
		Calendar.click();
}

@FindBy(how= How.ID, using = "List")
	public static WebElement List;

public void verify_List(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(List.getAttribute("value"),data);
	}

}

public void verify_List_Status(String data){
		//Verifies the Status of the List
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(List.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(List.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!List.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!List.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_List(String data){
		List.click();
}

@FindBy(how= How.ID, using = "Button")
	public static WebElement Button;

public void verify_Button_Status(String data){
		//Verifies the Status of the Button
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Button.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Button.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Button.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Button.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Button(){
		Button.click();
}

@FindBy(how= How.ID, using = "Hyperlink")
	public static WebElement Hyperlink;

public void verify_Hyperlink_Status(String data){
		//Verifies the Status of the Hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Hyperlink(){
		Hyperlink.click();
}

@FindBy(how= How.ID, using = "Label")
	public static WebElement Label;

public void verify_Label(String data){
		Assert.assertEquals(Label,Label);
}

public void verify_Label_Status(String data){
		//Verifies the Status of the Label
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Label.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Label.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Label.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Label.isEnabled());
				break;
			default:
				break;
			}
		}
	}
@FindBy(how= How.ID, using = "Menus")
	public static WebElement Menus;

public void click_Menus(){
		Menus.click();
}

@FindBy(how= How.ID, using = "File")
	public static WebElement File;

public void click_File(){
		File.click();
}

@FindBy(how= How.ID, using = "Click")
	public static WebElement Click;

public void click_Click(){
		Click.click();
}

@FindBy(how= How.ID, using = "Mouse_Over")
	public static WebElement Mouse_Over;

public void click_Mouse_Over(){
		Mouse_Over.click();
}

@FindBy(how= How.ID, using = "SubMenu")
	public static WebElement SubMenu;

public void click_SubMenu(){
		SubMenu.click();
}

@FindBy(how= How.ID, using = "Tab1")
	public static WebElement Tab1;

public void click_Tab1(){
		Tab1.click();
}

@FindBy(how= How.ID, using = "Tab2")
	public static WebElement Tab2;

public void click_Tab2(){
		Tab2.click();
}

@FindBy(how= How.ID, using = "Group")
	public static WebElement Group;

public void click_Group(){
		Group.click();
}

@FindBy(how= How.ID, using = "Day1")
	public static WebElement Day1;

public void click_Day1(){
		Day1.click();
}

@FindBy(how= How.ID, using = "DC")
	public static WebElement DC;

public void click_DC(){
		DC.click();
}

@FindBy(how= How.ID, using = "ExcelScripter")
	public static WebElement ExcelScripter;

public void click_ExcelScripter(){
		ExcelScripter.click();
}

@FindBy(how= How.ID, using = "Model")
	public static WebElement Model;

public void click_Model(){
		Model.click();
}

@FindBy(how= How.ID, using = "AD")
	public static WebElement AD;

public void click_AD(){
		AD.click();
}

@FindBy(how= How.ID, using = "SD")
	public static WebElement SD;

public void click_SD(){
		SD.click();
}

@FindBy(how= How.ID, using = "SL")
	public static WebElement SL;

public void click_SL(){
		SL.click();
}

@FindBy(how= How.ID, using = "RB_Country")
	public WebElement RB_Country;

public void verify_RB_Country_Status(String data){
		//Verifies the Status of the RB_Country
		Assert.assertEquals(RB_Country,RB_Country);
}

public void select_RB_Country(){
		RB_Country.click();
}

@FindBy(how= How.ID, using = "RB_Canada")
	public WebElement RB_Canada;

public void verify_RB_Canada_Status(String data){
		//Verifies the Status of the RB_Canada
		Assert.assertEquals(RB_Canada,RB_Canada);
}

public void select_RB_Canada(){
		RB_Canada.click();
}

@FindBy(how= How.ID, using = "RB_India")
	public WebElement RB_India;

public void verify_RB_India_Status(String data){
		//Verifies the Status of the RB_India
		Assert.assertEquals(RB_India,RB_India);
}

public void select_RB_India(){
		RB_India.click();
}

@FindBy(how= How.ID, using = "RB_F")
	public WebElement RB_F;

public void verify_RB_F_Status(String data){
		//Verifies the Status of the RB_F
		Assert.assertEquals(RB_F,RB_F);
}

public void select_RB_F(){
		RB_F.click();
}

@FindBy(how= How.ID, using = "RB_M")
	public WebElement RB_M;

public void verify_RB_M_Status(String data){
		//Verifies the Status of the RB_M
		Assert.assertEquals(RB_M,RB_M);
}

public void select_RB_M(){
		RB_M.click();
}

@FindBy(how= How.ID, using = "RB_Open_New_Account")
	public WebElement RB_Open_New_Account;

public void verify_RB_Open_New_Account_Status(String data){
		//Verifies the Status of the RB_Open_New_Account
		Assert.assertEquals(RB_Open_New_Account,RB_Open_New_Account);
}

public void select_RB_Open_New_Account(){
		RB_Open_New_Account.click();
}

@FindBy(how= How.ID, using = "RB_Account_Type")
	public WebElement RB_Account_Type;

public void verify_RB_Account_Type_Status(String data){
		//Verifies the Status of the RB_Account_Type
		Assert.assertEquals(RB_Account_Type,RB_Account_Type);
}

public void select_RB_Account_Type(){
		RB_Account_Type.click();
}

@FindBy(how= How.ID, using = "RB_CHECKING")
	public WebElement RB_CHECKING;

public void verify_RB_CHECKING_Status(String data){
		//Verifies the Status of the RB_CHECKING
		Assert.assertEquals(RB_CHECKING,RB_CHECKING);
}

public void select_RB_CHECKING(){
		RB_CHECKING.click();
}

@FindBy(how= How.ID, using = "RB_SAVINGS")
	public WebElement RB_SAVINGS;

public void verify_RB_SAVINGS_Status(String data){
		//Verifies the Status of the RB_SAVINGS
		Assert.assertEquals(RB_SAVINGS,RB_SAVINGS);
}

public void select_RB_SAVINGS(){
		RB_SAVINGS.click();
}

@FindBy(how= How.ID, using = "RB_Existing_Account")
	public WebElement RB_Existing_Account;

public void verify_RB_Existing_Account_Status(String data){
		//Verifies the Status of the RB_Existing_Account
		Assert.assertEquals(RB_Existing_Account,RB_Existing_Account);
}

public void select_RB_Existing_Account(){
		RB_Existing_Account.click();
}

@FindBy(how= How.ID, using = "RB_12345")
	public WebElement RB_12345;

public void verify_RB_12345_Status(String data){
		//Verifies the Status of the RB_12345
		Assert.assertEquals(RB_12345,RB_12345);
}

public void select_RB_12345(){
		RB_12345.click();
}

@FindBy(how= How.ID, using = "RB_Transfer_Funds")
	public WebElement RB_Transfer_Funds;

public void verify_RB_Transfer_Funds_Status(String data){
		//Verifies the Status of the RB_Transfer_Funds
		Assert.assertEquals(RB_Transfer_Funds,RB_Transfer_Funds);
}

public void select_RB_Transfer_Funds(){
		RB_Transfer_Funds.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}