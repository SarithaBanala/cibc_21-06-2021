package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Order_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Order_name")
	public static WebElement Order_name;

public void verify_Order_name(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Order_name.getAttribute("value"),data);
	}

}

public void verify_Order_name_Status(String data){
		//Verifies the Status of the Order_name
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Order_name.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Order_name.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Order_name.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Order_name.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Order_name(String data){
		Order_name.clear();
		Order_name.sendKeys(data);
}

@FindBy(how= How.ID, using = "Qty")
	public static WebElement Qty;

public void verify_Qty(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Qty.getAttribute("value"),data);
	}

}

public void verify_Qty_Status(String data){
		//Verifies the Status of the Qty
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Qty.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Qty.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Qty.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Qty.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Qty(String data){
		Qty.clear();
		Qty.sendKeys(data);
}

@FindBy(how= How.ID, using = "Order_details_Country")
	public WebElement Order_details_Country;

public void verify_Order_details_Country_Status(String data){
		//Verifies the Status of the Order_details_Country
		Assert.assertEquals(Order_details_Country,Order_details_Country);
}

public void select_Order_details_Country(){
		Order_details_Country.click();
}

@FindBy(how= How.ID, using = "Order_details_Canada")
	public WebElement Order_details_Canada;

public void verify_Order_details_Canada_Status(String data){
		//Verifies the Status of the Order_details_Canada
		Assert.assertEquals(Order_details_Canada,Order_details_Canada);
}

public void select_Order_details_Canada(){
		Order_details_Canada.click();
}

@FindBy(how= How.ID, using = "Order_details_India")
	public WebElement Order_details_India;

public void verify_Order_details_India_Status(String data){
		//Verifies the Status of the Order_details_India
		Assert.assertEquals(Order_details_India,Order_details_India);
}

public void select_Order_details_India(){
		Order_details_India.click();
}

@FindBy(how= How.ID, using = "Order_details_Gender")
	public WebElement Order_details_Gender;

public void verify_Order_details_Gender_Status(String data){
		//Verifies the Status of the Order_details_Gender
		Assert.assertEquals(Order_details_Gender,Order_details_Gender);
}

public void select_Order_details_Gender(){
		Order_details_Gender.click();
}

@FindBy(how= How.ID, using = "Order_details_F")
	public WebElement Order_details_F;

public void verify_Order_details_F_Status(String data){
		//Verifies the Status of the Order_details_F
		Assert.assertEquals(Order_details_F,Order_details_F);
}

public void select_Order_details_F(){
		Order_details_F.click();
}

@FindBy(how= How.ID, using = "Order_details_M")
	public WebElement Order_details_M;

public void verify_Order_details_M_Status(String data){
		//Verifies the Status of the Order_details_M
		Assert.assertEquals(Order_details_M,Order_details_M);
}

public void select_Order_details_M(){
		Order_details_M.click();
}

@FindBy(how= How.ID, using = "Order_details_Open_New_Account")
	public WebElement Order_details_Open_New_Account;

public void verify_Order_details_Open_New_Account_Status(String data){
		//Verifies the Status of the Order_details_Open_New_Account
		Assert.assertEquals(Order_details_Open_New_Account,Order_details_Open_New_Account);
}

public void select_Order_details_Open_New_Account(){
		Order_details_Open_New_Account.click();
}

@FindBy(how= How.ID, using = "Order_details_Account_Type")
	public WebElement Order_details_Account_Type;

public void verify_Order_details_Account_Type_Status(String data){
		//Verifies the Status of the Order_details_Account_Type
		Assert.assertEquals(Order_details_Account_Type,Order_details_Account_Type);
}

public void select_Order_details_Account_Type(){
		Order_details_Account_Type.click();
}

@FindBy(how= How.ID, using = "Order_details_CHECKING")
	public WebElement Order_details_CHECKING;

public void verify_Order_details_CHECKING_Status(String data){
		//Verifies the Status of the Order_details_CHECKING
		Assert.assertEquals(Order_details_CHECKING,Order_details_CHECKING);
}

public void select_Order_details_CHECKING(){
		Order_details_CHECKING.click();
}

@FindBy(how= How.ID, using = "Order_details_SAVINGS")
	public WebElement Order_details_SAVINGS;

public void verify_Order_details_SAVINGS_Status(String data){
		//Verifies the Status of the Order_details_SAVINGS
		Assert.assertEquals(Order_details_SAVINGS,Order_details_SAVINGS);
}

public void select_Order_details_SAVINGS(){
		Order_details_SAVINGS.click();
}

@FindBy(how= How.ID, using = "Order_details_Existing_Account")
	public WebElement Order_details_Existing_Account;

public void verify_Order_details_Existing_Account_Status(String data){
		//Verifies the Status of the Order_details_Existing_Account
		Assert.assertEquals(Order_details_Existing_Account,Order_details_Existing_Account);
}

public void select_Order_details_Existing_Account(){
		Order_details_Existing_Account.click();
}

@FindBy(how= How.ID, using = "Order_details_12345")
	public WebElement Order_details_12345;

public void verify_Order_details_12345_Status(String data){
		//Verifies the Status of the Order_details_12345
		Assert.assertEquals(Order_details_12345,Order_details_12345);
}

public void select_Order_details_12345(){
		Order_details_12345.click();
}

@FindBy(how= How.ID, using = "Order_details_Transfer_Funds")
	public WebElement Order_details_Transfer_Funds;

public void verify_Order_details_Transfer_Funds_Status(String data){
		//Verifies the Status of the Order_details_Transfer_Funds
		Assert.assertEquals(Order_details_Transfer_Funds,Order_details_Transfer_Funds);
}

public void select_Order_details_Transfer_Funds(){
		Order_details_Transfer_Funds.click();
}

@FindBy(how= How.ID, using = "Order_details_Country")
	public WebElement Order_details_Country;

public void verify_Order_details_Country_Status(String data){
		//Verifies the Status of the Order_details_Country
		Assert.assertEquals(Order_details_Country,Order_details_Country);
}

public void select_Order_details_Country(){
		Order_details_Country.click();
}

@FindBy(how= How.ID, using = "Order_details_Canada")
	public WebElement Order_details_Canada;

public void verify_Order_details_Canada_Status(String data){
		//Verifies the Status of the Order_details_Canada
		Assert.assertEquals(Order_details_Canada,Order_details_Canada);
}

public void select_Order_details_Canada(){
		Order_details_Canada.click();
}

@FindBy(how= How.ID, using = "Order_details_India")
	public WebElement Order_details_India;

public void verify_Order_details_India_Status(String data){
		//Verifies the Status of the Order_details_India
		Assert.assertEquals(Order_details_India,Order_details_India);
}

public void select_Order_details_India(){
		Order_details_India.click();
}

@FindBy(how= How.ID, using = "Order_details_Gender")
	public WebElement Order_details_Gender;

public void verify_Order_details_Gender_Status(String data){
		//Verifies the Status of the Order_details_Gender
		Assert.assertEquals(Order_details_Gender,Order_details_Gender);
}

public void select_Order_details_Gender(){
		Order_details_Gender.click();
}

@FindBy(how= How.ID, using = "Order_details_F")
	public WebElement Order_details_F;

public void verify_Order_details_F_Status(String data){
		//Verifies the Status of the Order_details_F
		Assert.assertEquals(Order_details_F,Order_details_F);
}

public void select_Order_details_F(){
		Order_details_F.click();
}

@FindBy(how= How.ID, using = "Order_details_M")
	public WebElement Order_details_M;

public void verify_Order_details_M_Status(String data){
		//Verifies the Status of the Order_details_M
		Assert.assertEquals(Order_details_M,Order_details_M);
}

public void select_Order_details_M(){
		Order_details_M.click();
}

@FindBy(how= How.ID, using = "Order_details_Open_New_Account")
	public WebElement Order_details_Open_New_Account;

public void verify_Order_details_Open_New_Account_Status(String data){
		//Verifies the Status of the Order_details_Open_New_Account
		Assert.assertEquals(Order_details_Open_New_Account,Order_details_Open_New_Account);
}

public void select_Order_details_Open_New_Account(){
		Order_details_Open_New_Account.click();
}

@FindBy(how= How.ID, using = "Order_details_Account_Type")
	public WebElement Order_details_Account_Type;

public void verify_Order_details_Account_Type_Status(String data){
		//Verifies the Status of the Order_details_Account_Type
		Assert.assertEquals(Order_details_Account_Type,Order_details_Account_Type);
}

public void select_Order_details_Account_Type(){
		Order_details_Account_Type.click();
}

@FindBy(how= How.ID, using = "Order_details_CHECKING")
	public WebElement Order_details_CHECKING;

public void verify_Order_details_CHECKING_Status(String data){
		//Verifies the Status of the Order_details_CHECKING
		Assert.assertEquals(Order_details_CHECKING,Order_details_CHECKING);
}

public void select_Order_details_CHECKING(){
		Order_details_CHECKING.click();
}

@FindBy(how= How.ID, using = "Order_details_SAVINGS")
	public WebElement Order_details_SAVINGS;

public void verify_Order_details_SAVINGS_Status(String data){
		//Verifies the Status of the Order_details_SAVINGS
		Assert.assertEquals(Order_details_SAVINGS,Order_details_SAVINGS);
}

public void select_Order_details_SAVINGS(){
		Order_details_SAVINGS.click();
}

@FindBy(how= How.ID, using = "Order_details_Existing_Account")
	public WebElement Order_details_Existing_Account;

public void verify_Order_details_Existing_Account_Status(String data){
		//Verifies the Status of the Order_details_Existing_Account
		Assert.assertEquals(Order_details_Existing_Account,Order_details_Existing_Account);
}

public void select_Order_details_Existing_Account(){
		Order_details_Existing_Account.click();
}

@FindBy(how= How.ID, using = "Order_details_12345")
	public WebElement Order_details_12345;

public void verify_Order_details_12345_Status(String data){
		//Verifies the Status of the Order_details_12345
		Assert.assertEquals(Order_details_12345,Order_details_12345);
}

public void select_Order_details_12345(){
		Order_details_12345.click();
}

@FindBy(how= How.ID, using = "Order_details_Transfer_Funds")
	public WebElement Order_details_Transfer_Funds;

public void verify_Order_details_Transfer_Funds_Status(String data){
		//Verifies the Status of the Order_details_Transfer_Funds
		Assert.assertEquals(Order_details_Transfer_Funds,Order_details_Transfer_Funds);
}

public void select_Order_details_Transfer_Funds(){
		Order_details_Transfer_Funds.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}