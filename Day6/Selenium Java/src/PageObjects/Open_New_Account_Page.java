package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Open_New_Account_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Account_Tupe")
	public static WebElement Account_Tupe;

public void verify_Account_Tupe(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Account_Tupe.getAttribute("value"),data);
	}

}

public void verify_Account_Tupe_Status(String data){
		//Verifies the Status of the Account_Tupe
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Account_Tupe.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Account_Tupe.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Account_Tupe.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Account_Tupe.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Account_Tupe(String data){
		Select dropdown= new Select(Account_Tupe);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "Existing_Account")
	public static WebElement Existing_Account;

public void verify_Existing_Account(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Existing_Account.getAttribute("value"),data);
	}

}

public void verify_Existing_Account_Status(String data){
		//Verifies the Status of the Existing_Account
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Existing_Account.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Existing_Account.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Existing_Account.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Existing_Account.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Existing_Account(String data){
		Select dropdown= new Select(Existing_Account);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "OPEN_NEW_ACCOUNT")
	public static WebElement OPEN_NEW_ACCOUNT;

public void verify_OPEN_NEW_ACCOUNT_Status(String data){
		//Verifies the Status of the OPEN_NEW_ACCOUNT
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(OPEN_NEW_ACCOUNT.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(OPEN_NEW_ACCOUNT.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!OPEN_NEW_ACCOUNT.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!OPEN_NEW_ACCOUNT.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_OPEN_NEW_ACCOUNT(){
		OPEN_NEW_ACCOUNT.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Country")
	public WebElement Open_New_Account_Country;

public void verify_Open_New_Account_Country_Status(String data){
		//Verifies the Status of the Open_New_Account_Country
		Assert.assertEquals(Open_New_Account_Country,Open_New_Account_Country);
}

public void select_Open_New_Account_Country(){
		Open_New_Account_Country.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Canada")
	public WebElement Open_New_Account_Canada;

public void verify_Open_New_Account_Canada_Status(String data){
		//Verifies the Status of the Open_New_Account_Canada
		Assert.assertEquals(Open_New_Account_Canada,Open_New_Account_Canada);
}

public void select_Open_New_Account_Canada(){
		Open_New_Account_Canada.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_India")
	public WebElement Open_New_Account_India;

public void verify_Open_New_Account_India_Status(String data){
		//Verifies the Status of the Open_New_Account_India
		Assert.assertEquals(Open_New_Account_India,Open_New_Account_India);
}

public void select_Open_New_Account_India(){
		Open_New_Account_India.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Gender")
	public WebElement Open_New_Account_Gender;

public void verify_Open_New_Account_Gender_Status(String data){
		//Verifies the Status of the Open_New_Account_Gender
		Assert.assertEquals(Open_New_Account_Gender,Open_New_Account_Gender);
}

public void select_Open_New_Account_Gender(){
		Open_New_Account_Gender.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_F")
	public WebElement Open_New_Account_F;

public void verify_Open_New_Account_F_Status(String data){
		//Verifies the Status of the Open_New_Account_F
		Assert.assertEquals(Open_New_Account_F,Open_New_Account_F);
}

public void select_Open_New_Account_F(){
		Open_New_Account_F.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_M")
	public WebElement Open_New_Account_M;

public void verify_Open_New_Account_M_Status(String data){
		//Verifies the Status of the Open_New_Account_M
		Assert.assertEquals(Open_New_Account_M,Open_New_Account_M);
}

public void select_Open_New_Account_M(){
		Open_New_Account_M.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Open_New_Account")
	public WebElement Open_New_Account_Open_New_Account;

public void verify_Open_New_Account_Open_New_Account_Status(String data){
		//Verifies the Status of the Open_New_Account_Open_New_Account
		Assert.assertEquals(Open_New_Account_Open_New_Account,Open_New_Account_Open_New_Account);
}

public void select_Open_New_Account_Open_New_Account(){
		Open_New_Account_Open_New_Account.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_CHECKING")
	public WebElement Open_New_Account_CHECKING;

public void verify_Open_New_Account_CHECKING_Status(String data){
		//Verifies the Status of the Open_New_Account_CHECKING
		Assert.assertEquals(Open_New_Account_CHECKING,Open_New_Account_CHECKING);
}

public void select_Open_New_Account_CHECKING(){
		Open_New_Account_CHECKING.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_SAVINGS")
	public WebElement Open_New_Account_SAVINGS;

public void verify_Open_New_Account_SAVINGS_Status(String data){
		//Verifies the Status of the Open_New_Account_SAVINGS
		Assert.assertEquals(Open_New_Account_SAVINGS,Open_New_Account_SAVINGS);
}

public void select_Open_New_Account_SAVINGS(){
		Open_New_Account_SAVINGS.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Existing_Account")
	public WebElement Open_New_Account_Existing_Account;

public void verify_Open_New_Account_Existing_Account_Status(String data){
		//Verifies the Status of the Open_New_Account_Existing_Account
		Assert.assertEquals(Open_New_Account_Existing_Account,Open_New_Account_Existing_Account);
}

public void select_Open_New_Account_Existing_Account(){
		Open_New_Account_Existing_Account.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_12345")
	public WebElement Open_New_Account_12345;

public void verify_Open_New_Account_12345_Status(String data){
		//Verifies the Status of the Open_New_Account_12345
		Assert.assertEquals(Open_New_Account_12345,Open_New_Account_12345);
}

public void select_Open_New_Account_12345(){
		Open_New_Account_12345.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Transfer_Funds")
	public WebElement Open_New_Account_Transfer_Funds;

public void verify_Open_New_Account_Transfer_Funds_Status(String data){
		//Verifies the Status of the Open_New_Account_Transfer_Funds
		Assert.assertEquals(Open_New_Account_Transfer_Funds,Open_New_Account_Transfer_Funds);
}

public void select_Open_New_Account_Transfer_Funds(){
		Open_New_Account_Transfer_Funds.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Country")
	public WebElement Open_New_Account_Country;

public void verify_Open_New_Account_Country_Status(String data){
		//Verifies the Status of the Open_New_Account_Country
		Assert.assertEquals(Open_New_Account_Country,Open_New_Account_Country);
}

public void select_Open_New_Account_Country(){
		Open_New_Account_Country.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Canada")
	public WebElement Open_New_Account_Canada;

public void verify_Open_New_Account_Canada_Status(String data){
		//Verifies the Status of the Open_New_Account_Canada
		Assert.assertEquals(Open_New_Account_Canada,Open_New_Account_Canada);
}

public void select_Open_New_Account_Canada(){
		Open_New_Account_Canada.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_India")
	public WebElement Open_New_Account_India;

public void verify_Open_New_Account_India_Status(String data){
		//Verifies the Status of the Open_New_Account_India
		Assert.assertEquals(Open_New_Account_India,Open_New_Account_India);
}

public void select_Open_New_Account_India(){
		Open_New_Account_India.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Gender")
	public WebElement Open_New_Account_Gender;

public void verify_Open_New_Account_Gender_Status(String data){
		//Verifies the Status of the Open_New_Account_Gender
		Assert.assertEquals(Open_New_Account_Gender,Open_New_Account_Gender);
}

public void select_Open_New_Account_Gender(){
		Open_New_Account_Gender.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_F")
	public WebElement Open_New_Account_F;

public void verify_Open_New_Account_F_Status(String data){
		//Verifies the Status of the Open_New_Account_F
		Assert.assertEquals(Open_New_Account_F,Open_New_Account_F);
}

public void select_Open_New_Account_F(){
		Open_New_Account_F.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_M")
	public WebElement Open_New_Account_M;

public void verify_Open_New_Account_M_Status(String data){
		//Verifies the Status of the Open_New_Account_M
		Assert.assertEquals(Open_New_Account_M,Open_New_Account_M);
}

public void select_Open_New_Account_M(){
		Open_New_Account_M.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Open_New_Account")
	public WebElement Open_New_Account_Open_New_Account;

public void verify_Open_New_Account_Open_New_Account_Status(String data){
		//Verifies the Status of the Open_New_Account_Open_New_Account
		Assert.assertEquals(Open_New_Account_Open_New_Account,Open_New_Account_Open_New_Account);
}

public void select_Open_New_Account_Open_New_Account(){
		Open_New_Account_Open_New_Account.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Account_Type")
	public WebElement Open_New_Account_Account_Type;

public void verify_Open_New_Account_Account_Type_Status(String data){
		//Verifies the Status of the Open_New_Account_Account_Type
		Assert.assertEquals(Open_New_Account_Account_Type,Open_New_Account_Account_Type);
}

public void select_Open_New_Account_Account_Type(){
		Open_New_Account_Account_Type.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_CHECKING")
	public WebElement Open_New_Account_CHECKING;

public void verify_Open_New_Account_CHECKING_Status(String data){
		//Verifies the Status of the Open_New_Account_CHECKING
		Assert.assertEquals(Open_New_Account_CHECKING,Open_New_Account_CHECKING);
}

public void select_Open_New_Account_CHECKING(){
		Open_New_Account_CHECKING.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_SAVINGS")
	public WebElement Open_New_Account_SAVINGS;

public void verify_Open_New_Account_SAVINGS_Status(String data){
		//Verifies the Status of the Open_New_Account_SAVINGS
		Assert.assertEquals(Open_New_Account_SAVINGS,Open_New_Account_SAVINGS);
}

public void select_Open_New_Account_SAVINGS(){
		Open_New_Account_SAVINGS.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_12345")
	public WebElement Open_New_Account_12345;

public void verify_Open_New_Account_12345_Status(String data){
		//Verifies the Status of the Open_New_Account_12345
		Assert.assertEquals(Open_New_Account_12345,Open_New_Account_12345);
}

public void select_Open_New_Account_12345(){
		Open_New_Account_12345.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Transfer_Funds")
	public WebElement Open_New_Account_Transfer_Funds;

public void verify_Open_New_Account_Transfer_Funds_Status(String data){
		//Verifies the Status of the Open_New_Account_Transfer_Funds
		Assert.assertEquals(Open_New_Account_Transfer_Funds,Open_New_Account_Transfer_Funds);
}

public void select_Open_New_Account_Transfer_Funds(){
		Open_New_Account_Transfer_Funds.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}