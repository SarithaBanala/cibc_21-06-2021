com.conformiq.creator.structure.v15
creator.gui.screen qmla776f103537841d2927b5f6ce2dc36a5 "Parabank Login"
{
	creator.gui.form qmlc42ccce7d92646d7a9fc2446ad1c3782 "Customer Login"
	{
		creator.gui.textbox qml14c74626ef92402593f55ecff95f0d69 "Username"
	annotations = ["selenium:xpath" = "";]
			type = String
			status = dontcare;
		creator.gui.textbox qmld55c7761a04f455a831fda668dd73b72 "passowrd"
			type = String
			status = dontcare;
	}
	creator.gui.button qml6591705e1d034eb6a01ade141f6dea78 "Login"
		status = dontcare
		deleted;
	creator.gui.labelwidget qml7a574bac31814b008c2f16322adced54 "Error Message"
		status = dontcare;
	creator.gui.button qml1a19ef2ca71a4300b39a65bb1da7209c "Login"
		status = dontcare;
}
creator.gui.screen qml152d66426dc44fe0b94fb99c76426b95 "Account Services"
{
	creator.gui.hyperlink qml113f567bb83542be8a5a517c2d019a51 "Open New Account"
		status = dontcare;
	creator.gui.hyperlink qmldab936b376a74136b8601027615e82a6 "Transfer Funds"
		status = dontcare;
	creator.gui.hyperlink qml6f01d1ddb73044278bb4711e156e946d "Logout"
		status = dontcare;
}
creator.gui.screen qmlacede726ae0d4d8487ddd39421021912 "Screen Widget"
{
	creator.gui.form qml68e3428eb14242dcbf904b8e7ed6be4e "Form Widget"
	{
		creator.gui.textbox qmlaebb18c825394d1d877c2f007b206578 "TB"
			type = String
			status = dontcare;
		creator.gui.checkbox qmlc0370215ef4a44f2a633a53b67613a63 "CB"
			status = dontcare
			checked = dontcare;
		creator.gui.dropdown qml4da14780d9ea443387f6a54a053ec491 "DD"
			type = qml5bec9cc313a7415b8e9f6e3e7d1e1b61
			status = dontcare;
		creator.gui.radiobutton qmlf4b6e46f6a7a445f937d0c018091af41 "RB"
	annotations = ["selenium:" = "";"selenium:" = "";]
			type = qmld2f1b77a83c24b3e8c687cb43e0b844a
			status = dontcare;
		creator.gui.calendar qml0166e680aa7d4cfe9ce5faba2c3a8740 "Calendar"
			status = dontcare;
		creator.gui.listbox qml69b80989065c40aaa57fc875b1763281 "List"
			status = dontcare
			items = [ ];
	}
	creator.gui.button qmlc494347cdd474e38a19d94da5b230eb9 "Button"
		status = dontcare;
	creator.gui.hyperlink qml794822bcec544abc87d11fe17fdfeeeb "Hyperlink"
		status = dontcare;
	creator.gui.labelwidget qml712afedfd86b4bc3a2f6fa28770ece5b "Label"
		status = dontcare;
	creator.gui.menubar qml91e7a230ec214914a8efd3e6371186bd "Menus"
	{
		creator.gui.menu qml3fc08a89af4547b58d5193640c9f815e "File"
		{
			creator.gui.clickchoice qml7616d0621a0145bc9fdafaca68a02ff7 "Click";
			creator.gui.mouseoverchoice qml3ee43345842d414ab58b9256d9e9429e
			"Mouse Over";
			creator.gui.menu qml3dddef965cc64ad4aa2d3bb66b9d8976 "SubMenu"
			{
			}
		}
	}
	creator.gui.tab qml00d6b23050d14ec29a29c14d6e4bf8d2 "Tab1"
		selected
	{
	}
	creator.gui.tab qml435976acd9f548f49d34c07abf533ee3 "Tab2"
	{
	}
	creator.gui.group qml73d7329794da4d089fb0019ad754711d "Group"
	{
	}
	creator.gui.treenode qml42a9de6dcc054b819df05256d1d6bb11 "Day1"
	{
		creator.gui.treenode qml7a5f2da5ff5a49eba35e33c9be03704a "DC"
		{
			creator.gui.treenode qml1432e50a271942fda63f2fcfe169a0d5 "ExcelScripter"
			{
			}
		}
		creator.gui.treenode qmlf2f235429a054246b5812e8eaf7585ea "Model"
		{
			creator.gui.treenode qmle8f737c44de14780b6c84ec70b795d7f "AD"
			{
			}
			creator.gui.treenode qmld3d1c994a1114b038f3207ef886d2c7c "SD"
			{
			}
			creator.gui.treenode qml2b255ebd48c740b8992fada79868455d "SL"
			{
			}
		}
	}
}
creator.enum qml5bec9cc313a7415b8e9f6e3e7d1e1b61 "Country"
{
	creator.enumerationvalue qmlb0d050300ff1411a96285f414c47f815 "Canada";
	creator.enumerationvalue qmlcc413142c60c409ba647908f5a45dc3c "India";
}
creator.enum qmld2f1b77a83c24b3e8c687cb43e0b844a "Gender"
{
	creator.enumerationvalue qmlfc7f6374ebf44451a073c24e723ed13f "F";
	creator.enumerationvalue qml78b89550eb1744ce94da22f8a6234a44 "M";
}
creator.gui.screen qmlf93454817b6d4dc28fbf707a3f148a20 "Open New Account"
{
	creator.gui.form qml1f643c0e4ceb4cf4a0e3e5dd99b5f0a0 "Open New Account"
	{
		creator.gui.dropdown qml946643ae040e4b6cae8295abf9a5dc8a "Account Tupe"
			type = qml7daf9ff46fe344ebbd6fe301423bbe50
			status = dontcare;
		creator.gui.dropdown qmlf1183519882b4c0c8472bd26df92873d "Existing Account"
			type = qml0059477bb0764597b1f2e6f883cc4128
			status = dontcare;
	}
	creator.gui.button qmlb48cebf0c8bf4b6b8dfe5762301ee336 "OPEN NEW ACCOUNT"
		status = dontcare;
}
creator.enum qml7daf9ff46fe344ebbd6fe301423bbe50 "Account Type"
{
	creator.enumerationvalue qml3899454a37ca4dc09d661f2c444df253 "CHECKING";
	creator.enumerationvalue qml79006e54702743c0afce44a64ac9d579 "SAVINGS";
}
creator.enum qml0059477bb0764597b1f2e6f883cc4128 "Existing Account"
{
	creator.enumerationvalue qmlbdbb9bc8a17c4a6ea5fabde78d4dd371 "12345";
}
creator.gui.screen qml716d87941f4043a6a5f4ea7e81d1e092 "Transfer Funds"
{
	creator.gui.form qmlebc16a14110f44ac9de6b3ec9b148280 "Transfer Funds"
	{
		creator.gui.textbox qmld10f8689d19e4616a45bb2a82963ec2c "Amount"
			type = String
			status = dontcare;
		creator.gui.dropdown qml1b254487ccae426eb7b8fc4ff9855477 "From Account"
			type = qml0059477bb0764597b1f2e6f883cc4128
			status = dontcare;
		creator.gui.dropdown qmle4a5ca4d266e4ab08207686a037451ff "To Account"
			type = qml0059477bb0764597b1f2e6f883cc4128
			status = dontcare;
	}
	creator.gui.button qml9679ca1410d04909bb9886e40bba54a9 "Transfer Funds"
		status = dontcare;
}
creator.gui.screen qml2b482fc369794ecbb386097ae46f4f1d "Order"
{
	creator.gui.form qml07514aa2350e45929763c2dc008a1d88 "Order details"
	{
		creator.gui.textbox qmla5e13c4715064bd39db8e28afec29cb2 "Order name"
			type = String
			status = dontcare;
		creator.gui.textbox qmleb6209da0f71473eb3dd35f5ce3ccd79 "Qty"
			type = String
			status = dontcare;
	}
}
creator.message qml9fcefa0797244ca5bd20a8fc0cc09af8 "unnamed"
	deleted
{
}
creator.gui.screen qml9ce03795f668475191d1a15aed5b79a7 "Updates"
{
}